# -*- coding: utf8 -*-
# (C) Fabrice Sincère
# MicroPython

# STM32WB55 Nucleo dongle
# MicroPython v1.12-510-g1e6d18c91 on 2020-06-08;
# USBDongle-WB55 with STM32WB55CGU6

# DS1631 : 4 wires
# 3.3 V
# GND
# PB9 pin -> SDA  + r pullup 2.2k
# PB8 pin -> SCL  + r pullup 2.2k

from machine import Pin, I2C
import time
import DS1631

# i2c bus pins
SDA = Pin.cpu.B9
SCL = Pin.cpu.B8
i2c = I2C(scl=Pin(SCL), sda=Pin(SDA))

# i2c bus scan
ds1631_address_list = [i for i in i2c.scan() if 0x48 <= i <= 0x4F]
[print(hex(i)) for i in ds1631_address_list]

i2c_address = ds1631_address_list[0]
ic1 = DS1631.DS1631(i2c, i2c_address)
# thermostat config
ic1.set_tout_polarity("active-low")
ic1.set_thigh(24.5)
ic1.set_tlow(22.5)
# thermometer config
ic1.set_conversion_mode("continuous")
ic1.set_resolution(12)
ic1.start_convert()
# read temperature
while True:
    time.sleep_ms(750)
    temperature = ic1.get_temperature()
    print("Temperature  : {} °C".format(temperature))
