# -*- coding: utf8 -*-
# (C) Fabrice Sincère

# STM32L476 Nucleo-64
# MicroPython v1.17 on NUCLEO-L476RG with STM32L476RG

# DS1631/DS1621 : 4 wires
# 3.3 V
# GND
# D14 pin -> SDA  + r pullup 2.2k
# D15 pin -> SDL  + r pullup 2.2k

# test : OK

from machine import Pin, I2C
import time
import DS1631

__version__ = (0, 0, 2)
__author__ = "Fabrice Sincère <fabrice.sincere@wanadoo.fr>"

# i2c bus pins
i2c = I2C(1)  # D14 pin -> SDA ; D15 pin -> SDL
# i2c = I2C(2)  # B11 (morpho) pin -> SDA ; D6 (arduino) pin -> SDL
# i2c = I2C(3)  # A4 (arduino) pin -> SDA ; A5 (arduino) pin -> SDL

# i2c bus scan
ds1631_address_list = [i for i in i2c.scan() if 0x48 <= i <= 0x4F]
[print(hex(i)) for i in ds1631_address_list]

i2c_address = ds1631_address_list[0]
ic1 = DS1631.DS1631(i2c, i2c_address)

# thermostat config
ic1.set_tout_polarity("active-low")
ic1.set_thigh(24.5)
ic1.set_tlow(22.5)

# thermometer config
ic1.set_conversion_mode("continuous")
ic1.set_resolution(12)
ic1.start_convert()

# read temperature
while True:
    time.sleep_ms(750)
    temperature = ic1.get_temperature()
    print("Temperature  : {} °C".format(temperature))
