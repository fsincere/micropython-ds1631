# -*- coding: utf8 -*-
# (C) Fabrice Sincère
# MicroPython

# STM32_Discovery-kit_B_L475E_IOT01A

# MicroPython v1.12-510-g1e6d18c91 on 2020-06-08; B-L475E-IOT01A with STM32L475

# DS1631 : 4 wires
# 3.3 V
# GND
# D14 pin -> SDA  + r pullup 2.2k
# D15 pin -> SCL  + r pullup 2.2k

from machine import Pin, I2C
import time
import DS1631

# i2c bus pins
D14 = Pin.cpu.B9
D15 = Pin.cpu.B8
i2c = I2C(scl=Pin(D15), sda=Pin(D14))

# i2c bus scan
ds1631_address_list = [i for i in i2c.scan() if 0x48 <= i <= 0x4F]
[print(hex(i)) for i in ds1631_address_list]

i2c_address = ds1631_address_list[0]
ic1 = DS1631.DS1631(i2c, i2c_address)
# thermostat config
ic1.set_tout_polarity("active-low")
ic1.set_thigh(24.5)
ic1.set_tlow(22.5)
# thermometer config
ic1.set_conversion_mode("continuous")
ic1.set_resolution(12)
ic1.start_convert()
# read temperature
while True:
    time.sleep_ms(750)
    temperature = ic1.get_temperature()
    print("Temperature  : {} °C".format(temperature))
