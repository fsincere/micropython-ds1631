# -*- coding: utf8 -*-
# (C) Fabrice Sincère
# MicroPython

# ESP8266
# MicroPython v1.12 on 2019-12-20; ESP module with ESP8266

# DS1631 : 4 wires
# 3.3 V
# GND
# pin 4 <=> D2 pin board + r pullup 2.2k
# pin 5 <=> D1 pin board + r pullup 2.2k

from machine import Pin, I2C
import DS1631
import time

# i2c bus pins
i2c = I2C(scl=Pin(4), sda=Pin(5))
# i2c = I2C(scl=Pin(5), sda=Pin(4))  # software i2c

# i2c bus scan
[print(hex(i)) for i in i2c.scan()]

i2c_address = 0x49
ic1 = DS1631.DS1631(i2c, i2c_address)
# thermostat config
ic1.set_tout_polarity("active-low")
ic1.set_thigh(24.5)
ic1.set_tlow(22.5)
# thermometer config
ic1.set_conversion_mode("continuous")
ic1.set_resolution(12)
ic1.start_convert()
# read temperature
while True:
    time.sleep_ms(750)
    temperature = ic1.get_temperature()
    print("Temperature  : {} °C".format(temperature))
