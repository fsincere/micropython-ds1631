# -*- coding: utf8 -*-
# (C) Fabrice Sincère
# MicroPython

# STM32L476 Nucleo-64
# MicroPython v1.12 on 2019-12-20; NUCLEO-L476RG with STM32L476RG
# http://micropython.org/download

# DS1631 : 4 wires
# 3.3 V
# GND
# D14 pin -> SDA  + r pullup 2.2k
# D15 pin -> SDL  + r pullup 2.2k

from machine import Pin, I2C
import time
import DS1631

# i2c bus pins

D14 = Pin.cpu.B9
D15 = Pin.cpu.B8
i2c = I2C(scl=Pin(D15), sda=Pin(D14))

# i2c bus scan
ds1631_address_list = [i for i in i2c.scan() if 0x48 <= i <= 0x4F]
[print(hex(i)) for i in ds1631_address_list]

i2c_address = ds1631_address_list[0]
ic1 = DS1631.DS1631(i2c, i2c_address)

ic1.software_por()
ic1.stop_convert()

ic1.print_configuration()

assert ic1.get_temperature() == -60.0
assert ic1.get_resolution()[0] == 12
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is False

ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
assert ic1.get_temperature() != -60.0

for i in range(9, 12+1):
    ic1.set_resolution(i)
    assert ic1.get_resolution()[0] == i

for i in ["one-shot", "continuous"]:
    ic1.set_conversion_mode(i)
    assert ic1.get_conversion_mode() == i

for i in ["active-high", "active-low"]:
    ic1.set_tout_polarity(i)
    assert ic1.get_tout_polarity() == i

ic1.stop_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(125.0)
assert ic1.get_thigh() == 125.0
ic1.set_tlow(124.0)
assert ic1.get_tlow() == 124.0
ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is True
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(-54.0)
assert ic1.get_thigh() == -54.0
ic1.set_tlow(-55.0)
assert ic1.get_tlow() == -55.0
ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is True
assert ic1.is_tlow_flag_on() is False
input("Tout = 0\nEnter to continue...")

ic1.stop_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(125.0)
assert ic1.get_thigh() == 125.0
ic1.set_tlow(124.0)
assert ic1.get_tlow() == 124.0
ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is True
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
ic1.set_tout_polarity("active-high")
ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
print(ic1.get_temperature())
input("Tout = 0\nEnter to continue...")

ic1.stop_convert()
ic1.set_tout_polarity("active-low")
ic1.start_convert()
time.sleep_ms(int(ic1.get_resolution()[2]))
print(ic1.get_temperature())
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
ic1.set_conversion_mode("one-shot")
ic1.set_resolution(12)
time.sleep_ms(int(ic1.get_resolution()[2]))
count = 0
start = time.ticks_ms()
ic1.start_convert()
while ic1.is_temperature_conversion_in_progress():
    count += 1
end = time.ticks_ms()
print('conversion time', time.ticks_diff(end, start), 'ms')
print(count, 'i2c requests')
print(ic1.get_temperature())

ic1.stop_convert()
count = 0
start = time.ticks_us()
ic1.set_thigh(63.42)
while ic1.is_eeprom_busy():
    count += 1
end = time.ticks_us()
print('eeprom write time', time.ticks_diff(end, start), 'us')
print(count, 'i2c requests')
print(ic1.get_thigh())

ic1.stop_convert()
count = 0
start = time.ticks_us()
ic1.reset_tlow_flag()
while ic1.is_eeprom_busy():
    count += 1
end = time.ticks_us()
print('eeprom write time', time.ticks_diff(end, start), 'us')
print(count, 'i2c requests')

print("Test OK")
