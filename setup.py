#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup
import sdist_upip
import sys
# Remove current dir from sys.path, otherwise setuptools will peek up our
# module instead of system's.
sys.path.pop(0)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='micropython-ds1631',
      version='0.4.6',
      description='MicroPython i2c driver for Maxim-Dallas DS1621 \
DS1631 DS1631A DS1721 DS1731 digital thermometer and thermostat.',
      long_description=long_description,
      long_description_content_type="text/markdown",
      author='Fabrice Sincère',
      author_email='fabrice.sincere@ac-grenoble.fr',
      maintainer='Fabrice Sincère',
      maintainer_email='fabrice.sincere@ac-grenoble.fr',
      url='https://framagit.org/fsincere/micropython-ds1631',
      classifiers=[
          'Programming Language :: Python :: Implementation :: MicroPython',
          'Intended Audience :: Developers',
          'Topic :: System :: Hardware',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'],
      cmdclass={'sdist': sdist_upip.sdist},
      py_modules=['DS1631'])
